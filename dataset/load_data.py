from PIL import Image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def load_data(dataset_path, gender):
    img = Image.open(dataset_path)
    img_ndarray = np.asarray(img, dtype='float64') / 256
    faces = np.empty((100, 40 * 40))

    # 分別取出每張照片
    for row in range(10):
        for column in range(10):
            faces[10 * row + column] = np.ndarray.flatten(
                img_ndarray[row * 40: (row + 1) * 40, column * 40: (column + 1) * 40]
            )

    label = np.repeat(gender, 100)
    return faces, label


def get_gender_data(male_imgpath, female_imgpath):
    female_face, female_label = load_data(female_imgpath, 0)
    male_face, male_label = load_data(male_imgpath, 1)

    m_data = pd.DataFrame(male_face)
    m_data["gender"] = pd.Series(male_label)

    f_data = pd.DataFrame(female_face)
    f_data["gender"] = pd.Series(female_label)

    all_data = pd.concat((f_data, m_data)).reset_index(drop=True)
    return all_data


def get_face_data(path):
    img = Image.open(path)
    img_ndarray = np.asarray(img, dtype='float64') / 256
    faces = np.empty((80, 40 * 40))

    data = pd.DataFrame(columns=[i for i in range(1600)] + ["label"])

    # 分別取出每張照片
    for column in range(16):
        for row in range(5):
            faces[row + 5 * column] = np.ndarray.flatten(
                img_ndarray[row * 40: (row + 1) * 40, column * 40: (column + 1) * 40]
            )
            data.loc[row + 5 * column] = list(faces[row + 5 * column]) + [int(column)]
    data.label = data.label.astype(int)
    return data


if __name__ == "__main__":
    print(get_face_data("facesP1.bmp"))
    # all_data = get_gender_data("mP1.bmp", "fP1.bmp")
    # print(all_data)
