from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
import pandas as pd
import numpy as np

data = pd.read_csv("../dataset/Iris.csv")
data.species.replace(['Iris-setosa', 'Iris-versicolor', 'Iris-virginica'], [1, 2, 3], inplace=True)
X_train, X_validation, Y_train, Y_validation = train_test_split(data.iloc[:, :4], data.iloc[:, 4],
                                                                test_size=0.2,
                                                                random_state=0)

model_sk = GaussianNB(priors=None)

model_sk.fit(X_train, Y_train)
print(model_sk.predict(X_validation))
print(np.array(list(Y_validation.values)))

# print(predict_NB_gaussian_class(X_train, mu_list, std_list, pi_list))


# N = 100
# X = np.linspace(4, 8, N)
# Y = np.linspace(1.5, 5, N)
# X, Y = np.meshgrid(X, Y)
# print(list(itertools.chain(*mu_list)))
#
# print(np.ravel(X), np.ravel(Y))
# print(np.array([np.ravel(X), np.ravel(Y)]))

# zz = np.array([predict_NB_gaussian_class(np.array([xx, yy]).reshape(-1, 1), mu_list, std_list, pi_list)
#                for xx, yy in zip(np.ravel(X), np.ravel(Y))])
#
# Z = zz.reshape(X.shape)
#
# color_list = ['Blues', 'Greens', 'Reds']
# my_norm = colors.Normalize(vmin=-1., vmax=1.)
#
# g = sns.FacetGrid(data, hue="species", size=10, palette='colorblind').map(plt.scatter, "sepal length",
#                                                                           "sepal width", ).add_legend()
# my_ax = g.ax
# Plot the filled and boundary contours
# my_ax.contourf(X, Y, Z, 2, alpha=.1, colors=('blue', 'green', 'red'))
# my_ax.contour(X, Y, Z, 2, alpha=1, colors=('blue', 'green', 'red'))
#
# # Addd axis and title
# my_ax.set_xlabel('Sepal length')
# my_ax.set_ylabel('Sepal width')
# my_ax.set_title('Gaussian Naive Bayes decision boundaries')
#
# plt.show()