from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris, load_wine
from plot.plot_boundary import plot_decision_region

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class Perceptron:
    def __init__(self, eta=0.01, n_iter=10):
        self.eta = eta
        self.n_iter = n_iter
        self.w_ = None
        self.errors_ = []

    def fit(self, x, y):
        self.w_ = np.zeros(1+x.shape[1])
        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(x, y):
                update = self.eta * (target-self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)
            self.errors_.append(errors)
        return self

    def net_input(self, x):
        return np.dot(x, self.w_[1:]) + self.w_[0]

    def predict(self, x):
        return np.where(self.net_input(x) >= 0.0, 1, 0)


if __name__ == "__main__":
    # data = pd.read_csv("../dataset/Iris.csv")
    data = load_wine()

    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'],
                                                                    test_size=0.2,
                                                                    random_state=0)

    Y_train, Y_validation = np.where(Y_train == 0, 1, 0),  np.where(Y_validation == 0, 1, 0)

    model = Perceptron(eta=0.01, n_iter=500).fit(X_train[:, 3:5], Y_train)
    output = model.predict(X_validation[:, 3:5])
    print(np.mean(output == Y_validation))

    plot_decision_region(X_train[:, 3:5], Y_train, classfier=model)
    plt.xlabel('Ash')
    plt.ylabel('Magnesium ')
    plt.show()

    sns.set()
    plt.plot(model.errors_, '-b')
    plt.show()