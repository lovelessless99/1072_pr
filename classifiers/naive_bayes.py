from sklearn.model_selection import train_test_split
from scipy.stats import norm

import itertools
import pandas as pd
import numpy as np


class GaussianNB:
    def __init__(self):
        self.mu_list = []
        self.std_list = []
        self.pi_list = []

    def fit(self, X_train, Y_train):
        X_train, Y_train = pd.DataFrame(X_train), pd.DataFrame(Y_train, columns=['output'])
        train_data = pd.concat([X_train, Y_train], axis=1, join='inner')
        class_num = len(np.unique(Y_train))
        self.mu_list = np.split(train_data.groupby('output').mean().values, class_num)
        self.std_list = np.split(train_data.groupby('output').std().values, class_num)
        self.pi_list = train_data.iloc[:, -1].value_counts().values / len(train_data)

        self.mu_list = list(itertools.chain(*self.mu_list))
        self.std_list = list(itertools.chain(*self.std_list))

        return self

    def predict(self, X_validation):
        scores_list = []
        output = []
        classes = len(self.mu_list)

        for row in X_validation:
            for p in range(classes):
                score = self.pi_list[p]
                for index_row, val in enumerate(row.tolist()):
                    score *= norm.pdf(x=val, loc=self.mu_list[p][index_row], scale=self.std_list[p][index_row])

                scores_list.append(score)
            output.append(np.argmax(scores_list))
            scores_list.clear()

        return output


if __name__ == "__main__":
    from sklearn.datasets import load_iris, load_breast_cancer, load_wine
    data = load_iris()
    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'],
                                                                    test_size=0.2,
                                                                    random_state=0)

    # Y_train, Y_validation = np.where(Y_train == 0, 1, 0), np.where(Y_validation == 0, 1, 0)
    # output = GaussianNB().fit(X_train, Y_train).predict(X_validation)
    from plot.plot_boundary import plot_decision_region
    import matplotlib.pyplot as plt
    import seaborn as sns

    model = GaussianNB().fit(X_train, Y_train)
    plot_decision_region(X_train[:, 0:2], Y_train, classfier=model)
    sns.set()
    plt.xlabel('sepal length')
    plt.ylabel('sepal width')
    plt.show()

    # sns.set()
    # plt.plot(model.errors_, '-b')
    # plt.show()
    output = model.predict(X_validation)
    print(np.mean(Y_validation == output))