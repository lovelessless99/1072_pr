from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris, load_breast_cancer, load_wine
from scipy.stats import multivariate_normal

import itertools
import pandas as pd
import numpy as np


class BayesClassifier:
    def __init__(self):
        self.mu = None
        self.prior_list = None
        self.train_data = None
        self.class_num = 0

    def fit(self, X_train, Y_train):
        X_train1, Y_train1 = pd.DataFrame(X_train), pd.DataFrame(Y_train, columns=['output'])
        train_data = pd.concat([X_train1, Y_train1], axis=1, join='inner')
        mu = np.split(train_data.groupby('output').mean().values, len(np.unique(Y_train)))
        self.class_num = len(np.unique(Y_train))
        self.train_data = train_data
        self.mu = list(itertools.chain(*mu))
        self.prior_list = np.array(np.bincount(Y_train) / len(Y_train))[:, np.newaxis]
        return self

    def predict(self, X_validation):
        posterior = []
        for i in range(self.class_num):
            data = np.array(self.train_data.iloc[list(self.train_data.groupby('output').groups[i]), :-1])
            distance = np.array([k - self.mu[i] for k in data])
            sigma = np.dot(distance.T, distance) / np.shape(distance)[0]
            likelihood = multivariate_normal.pdf(X_validation, mean=self.mu[i], cov=sigma)
            posterior.append(likelihood * self.prior_list[i])

        return [np.argmax(poster) for poster in np.array(posterior).T]


if __name__ == "__main__":
    data = load_iris()
    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'], test_size=0.2,
                                                                    random_state=0)

    from plot.plot_boundary import plot_decision_region
    import matplotlib.pyplot as plt
    import seaborn as sns

    model = BayesClassifier().fit(X_train[:, 0:2], Y_train)
    plot_decision_region(X_train[:, 0:2], Y_train, classfier=model)
    sns.set()
    plt.xlabel('sepal length')
    plt.ylabel('sepal width')
    plt.show()

    output = model.predict(X_validation[:, 0:2])
    print(np.mean(Y_validation == output))