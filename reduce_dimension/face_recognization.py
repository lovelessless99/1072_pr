from dataset.load_data import get_face_data
from reduce_dimension.PCA import PCA
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import random

# def face_recognization(data):


def split_data(data_path):
    data = get_face_data(data_path)
    test_data = pd.DataFrame(columns=list(data))

    chosen_list = []
    for i in range(16):
        chosen_index = 5 * i + random.randint(0, 4)
        chosen_list.append(chosen_index)
        test_data.loc[i] = data.loc[chosen_index]

    test_data.label = test_data.label.astype(int)
    train_data = data.drop(data.index[chosen_list]).reset_index(drop=True)
    return train_data, test_data

def eigenface(data, feature_num):
    mean_face = data.iloc[:, :-1].mean()
    data.iloc[:, :-1] = data.drop(columns=["label"]) - mean_face
    cov_matrix = np.cov(data.iloc[:, :-1])
    eigenvalues, eigenvectors, = np.linalg.eig(cov_matrix)
    eig_pairs = [(eigenvalues[index], eigenvectors[:, index]) for index in range(len(eigenvalues))]

    # Sort the eigen pairs in descending order:
    eig_pairs.sort(reverse=True)
    eigvalues_sort = [eig_pairs[index][0] for index in range(len(eigenvalues))]
    eigvectors_sort = [eig_pairs[index][1].real for index in range(len(eigenvalues))]
    return eigvectors_sort[:feature_num]


if __name__ == "__main__":
    train_data, test_data = split_data("../dataset/facesP1.bmp")
    train_sectors = train_data.groupby("label")
    test_sectors = test_data.groupby("label")
    feature_num = 7

    # PCA_train, PCA_test = [], []
    # for i in range(16):
    #     train = train_sectors.get_group(i)
    #     w = eigenface(train, feature_num)
    #     proj_data = np.dot(w, train.iloc[:, :-1]).T
    #     PCA_train_data = np.dot(train.iloc[:, :-1], proj_data)
    #     PCA_train.append(np.array(PCA_train_data))
    #
    #     test = test_sectors.get_group(i)
    #     PCA_test_data = np.dot(test.iloc[:, :-1], proj_data)
    #     PCA_test.append(np.array(PCA_test_data))
    #
    #     print("第 %s 輪完成 ! " % (i+1))
    #
    # PCA_TRAIN = np.vstack(tuple(PCA_train))
    # PCA_TEST = np.vstack(tuple(PCA_test))
    #
    # from classifiers.naive_bayes import GaussianNB
    #
    # model = GaussianNB().fit(np.array(PCA_TRAIN), np.array(train_data["label"]))
    # output = model.predict(PCA_TEST)
    # Y = np.array(test_data["label"])
    # print(output)
    # print(Y)
    # print(np.mean(output == Y))

    feature_num = 10
    w = eigenface(train_data, feature_num)
    proj_data = np.dot(w, train_data.iloc[:, :-1]).T

    PCA_train = np.dot(train_data.iloc[:, :-1], proj_data)
    PCA_test = np.dot(test_data.iloc[:, :-1], proj_data)

    from classifiers.naive_bayes import GaussianNB

    model = GaussianNB().fit(np.array(PCA_train), np.array(train_data["label"]))
    output = model.predict(PCA_test)
    Y = np.array(test_data["label"])
    print(np.mean(output == Y))





