import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def PCA(covariance):
    eigen_vals, eigen_vecs = np.linalg.eig(covariance)
    tot = sum(eigen_vals)
    var_exp = [(i / tot) for i in sorted(eigen_vals, reverse=True)]
    cum_var_exp = np.cumsum(var_exp)

    # var_exp, cum_var_exp = [i.real for i in var_exp], [j.real for j in cum_var_exp]
    print("各特徵值變異數比率：", var_exp)
    print("特徵值變異數比率累加：", cum_var_exp)

    # 畫圖 ：解釋變異數比率 ，各特徵值/特徵值總和
    # sns.set()
    #     # plt.bar(range(1, len(var_exp)+1), var_exp, alpha=0.5, align='center', label='individual explained variance')
    #     # plt.step(range(1, len(cum_var_exp)+1), cum_var_exp, where='mid', label='cumulative explained variance')
    #     # plt.ylabel('Explained variance ratio')
    #     # plt.xlabel('Principal components')
    #     # plt.legend(loc='best')
    #     # plt.tight_layout()
    #     # plt.show()
    # Make a list of (eigenvalue, eigenvector) tuples

    eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:, i])
                   for i in range(len(eigen_vals))]
    print("特徵值，特徵向量length：", len(eigen_pairs))
    print("特徵值，特徵向量：", eigen_pairs)
    # Sort the (eigenvalue, eigenvector) tuples from high to low
    eigen_pairs.sort(key=lambda k: k[0], reverse=True)
    print("特徵值，特徵向量排序：", eigen_pairs)
    w = np.hstack((eigen_pairs[0][1][:, np.newaxis], eigen_pairs[1][1][:, np.newaxis]))
    print('Matrix W:\n', w)
    return w


if __name__ == "__main__":
    from sklearn.model_selection import train_test_split
    from sklearn.datasets import load_iris, load_wine
    data = load_iris()
    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'], test_size=0.2,
                                                                    random_state=0)

    w = PCA(np.cov(X_train.T))
    X_train_pca = np.dot(X_train, w)
    X_validation_pca = np.dot(X_validation, w)
    sns.set()
    # colors = ['r', 'b', 'g']
    # markers = ['s', 'x', 'o']
    # for l, c, m in zip(np.unique(Y_train), colors, markers):
    #     plt.scatter(X_train_pca[Y_train == l, 0],
    #                 X_train_pca[Y_train == l, 1],
    #                 c=c, label=l, marker=m)
    #
    # plt.xlabel('PC 1')
    # plt.ylabel('PC 2')
    # plt.legend(loc='lower left')
    # plt.tight_layout()
    # plt.show()

    from plot.plot_boundary import plot_decision_region
    from classifiers.bayes_classifier import BayesClassifier

    model = BayesClassifier().fit(X_train_pca, Y_train)
    plot_decision_region(X_validation_pca, Y_validation, model)

    output = model.predict(X_validation_pca)
    print(np.mean(Y_validation == output))
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.legend(loc='lower right')
    plt.tight_layout()
    plt.show()
