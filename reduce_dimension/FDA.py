from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris, load_breast_cancer, load_wine
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def fisher(c_1, c_2):
    cov_1, u1 = c_1.cov(), c_1.mean()
    cov_2, u2 = c_2.cov(), c_2.mean()
    s_w = cov_1 + cov_2
    # s_w_inv = np.linalg.inv(s_w)
    u, s, v = np.linalg.svd(s_w)
    s_w_inv = np.dot(np.dot(v.T, np.linalg.inv(np.diag(s))), u.T)
    return np.dot(s_w_inv, u1 - u2)
    # u, s, v = np.linalg.svd(s_w)
    # s_w_inv = np.dot(np.dot(v.T, np.linalg.inv(np.diag(s))), u.T)


if __name__ == "__main__":
    # data = load_breast_cancer()
    raw_data = pd.read_csv("../dataset/heart.csv")
    data = {}
    data['data'] = np.array(raw_data.iloc[:, :-1])
    data['target'] = np.array(raw_data.iloc[:, -1])

    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'],
                                                                    test_size=0.2,
                                                                    random_state=0)
    # Y_train, Y_validation = np.where(Y_train == "male", 1, 0), np.where(Y_validation == "male", 1, 0)

    X_train1, Y_train1 = pd.DataFrame(X_train), pd.DataFrame(Y_train, columns=['output'])
    train_data = pd.concat([X_train1, Y_train1], axis=1, join='inner')
    c1, c2 = train_data.groupby('output').get_group(0).iloc[:, :-1], \
             train_data.groupby('output').get_group(1).iloc[:, :-1]

    w = fisher(c1, c2)

    train_proj_data = np.dot(X_train, w)
    test_proj_x = pd.DataFrame(np.dot(X_validation, w))

    # from classifiers.naive_bayes import GaussianNB
    from sklearn.naive_bayes import GaussianNB
    from classifiers.percetron import Perceptron
    # print(np.expand_dims(train_proj_data, axis=1).shape)
    # print(X_train.shape)
    train_proj_data = np.expand_dims(train_proj_data, axis=1)

    LDA_output = GaussianNB().fit(train_proj_data, Y_train).predict(test_proj_x)
    print(np.mean(Y_validation == LDA_output))

    output = GaussianNB().fit(X_train, Y_train).predict(X_validation)
    print(np.mean(Y_validation == output))


    from evaluation_model.evaluation import roc
    from sklearn import metrics
    # fpr1, tpr1 = roc(Y_validation, LDA_output)
    fpr1, tpr1, threshold = metrics.roc_curve(Y_validation, LDA_output)
    print(metrics.auc(fpr1, tpr1))
    # fpr2, tpr2 = roc(Y_validation, output)
    fpr2, tpr2, threshold = metrics.roc_curve(Y_validation, output)
    print(metrics.auc(fpr2, tpr2))


    plt.plot(fpr1, tpr1, color='darkorange', lw=2)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.show()

    plt.plot(fpr2, tpr2, color='green', lw=2)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    plt.show()





    # import matplotlib.pyplot as plt
    # import matplotlib
    # import seaborn as sns
    # val = 0
    # colors = ['red', 'blue']
    # sns.set()
    # proj_x = np.dot(X_validation, w)
    # plt.scatter(proj_x, np.zeros_like(proj_x) + val, c=Y_validation, cmap=matplotlib.colors.ListedColormap(colors))
    # plt.show()




    # out = judge(X_validation, w, c1, c2)
    # out = np.where(out == False, 1, 0)

    # print(np.mean(out == Y_validation))
    # print(out)
    # print(Y_validation)
    #
    # import matplotlib.pyplot as plt
    # import seaborn as sns
    #
    # x0 = 0
    # x1 = 1
    #
    # plt.scatter(c1.iloc[:, x0], c1.iloc[:, x1], c='#99CC99')
    # plt.scatter(c2.iloc[:, x0], c2.iloc[:, x1], c='#FFCC00')
    #
    # line_x = np.arange(min(np.min(c1.iloc[:, x0]), np.min(c2.iloc[:, x0])),
    #                    max(np.max(c1.iloc[:, x0]), np.max(c2.iloc[:, x0])),
    #                    step=1)
    # print(w)
    # sns.set()
    # # line_x = np.linspace(-10, 10)
    # # line_y = - (w[x0] * line_x) / w[x1]
    # line_y = np.dot(w[:4], line_x)
    # plt.plot(line_x, line_y, '-')
    # plt.show()
