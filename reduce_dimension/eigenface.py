from dataset.load_data import get_gender_data
import matplotlib.pyplot as plt
import numpy as np


def eigenface(data, feature_num):
    mean_face = data.iloc[:, :-1].mean()
    data.iloc[:, :-1] = data.drop(columns=["gender"]) - mean_face
    cov_matrix = np.cov(data.iloc[:, :-1])
    eigenvalues, eigenvectors, = np.linalg.eig(cov_matrix)
    eig_pairs = [(eigenvalues[index], eigenvectors[:, index]) for index in range(len(eigenvalues))]

    # Sort the eigen pairs in descending order:
    eig_pairs.sort(reverse=True)
    eigvalues_sort = [eig_pairs[index][0] for index in range(len(eigenvalues))]
    eigvectors_sort = [eig_pairs[index][1].real for index in range(len(eigenvalues))]
    return eigvectors_sort[:feature_num]
    # var_comp_sum = np.cumsum(eigvalues_sort) / sum(eigvalues_sort)
    #
    # # Show cumulative proportion of varaince with respect to components
    # print("Cumulative proportion of variance explained vector: \n%s" % var_comp_sum)
    #
    # # x-axis for number of principal components kept
    # num_comp = range(1, len(eigvalues_sort) + 1)
    # plt.title('Cum. Prop. Variance Explain and Components Kept')
    # plt.xlabel('Principal Components')
    # plt.ylabel('Cum. Prop. Variance Expalined')
    #
    # plt.scatter(num_comp, var_comp_sum)
    # plt.show()


if __name__ == "__main__":
    data = get_gender_data("../dataset/mP1.bmp", "../dataset/fP1.bmp")
    sectors = data.groupby("gender")
    train_size = 80

    male = sectors.get_group(1)
    female = sectors.get_group(0)

    # projection direction
    feature_num = 4
    w_male = eigenface(male.iloc[:train_size, :], feature_num)
    w_female = eigenface(female.iloc[:train_size, :], feature_num)

    male_proj_data = np.dot(w_male, male.iloc[:train_size, :-1]).T
    female_proj_data = np.dot(w_female, female.iloc[:train_size, :-1]).T

    # projection
    PCA_train_male, PCA_train_female = np.dot(male.iloc[:train_size, :-1], male_proj_data),\
                                       np.dot(female.iloc[:train_size, :-1], female_proj_data)

    PCA_test_male, PCA_test_female = np.dot(male.iloc[train_size:, :-1], male_proj_data), \
                                     np.dot(female.iloc[train_size:, :-1], female_proj_data)

    from classifiers.naive_bayes import GaussianNB
    import pandas as pd
    PCA_data = pd.DataFrame(np.concatenate((PCA_train_male, PCA_train_female)))
    PCA_data["label"] = pd.Series(np.concatenate((np.repeat(1, train_size), np.repeat(0, train_size))))

    model = GaussianNB().fit(np.array(PCA_data.iloc[:, :-1]), np.array(PCA_data.iloc[:, -1]))
    output = model.predict(np.concatenate((PCA_test_male, PCA_test_female)))
    Y = np.concatenate((np.array(male.iloc[train_size:, -1]), np.array(female.iloc[train_size:, -1])))
    print(np.mean(output == Y))



    # # mean face
    # male_mean_face = male.iloc[:, :-1].mean()
    # female_mean_face = female.iloc[:, :-1].mean()
    #
    # # male.iloc[:, :-1] -= male_mean_face
    # male.iloc[:, :-1] = male.drop(columns=["gender"]) - male_mean_face
    # female.iloc[:, :-1] = female.drop(columns=["gender"]) - female_mean_face
    #
    # cov_male_matrix = np.cov(male.drop(columns=["gender"]))
    # cov_female_matrix = np.cov(female.drop(columns=["gender"]))
    #
    # male_eigenvalues, male_eigenvectors, = np.linalg.eig(cov_male_matrix)
    # female_eigenvalues, female_eigenvectors, = np.linalg.eig(cov_female_matrix)


    # for i in range(10):
    #     img = np.array(male.iloc[i, :-1]).reshape(40, 40)
    #     plt.subplot(2, 5, 1 + i)
    #     plt.imshow(img, cmap='gray')
    #     plt.tick_params(labelleft='off', labelbottom='off', bottom='off', top='off', right='off', left='off',
    #                     which='both')
    # plt.show()

    # print(np.subtract(male.iloc[:, :-1], male_mean_face))
    # print(male)
    # print(female.iloc[:, :-1] - female_mean_face)