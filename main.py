import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', sep=',', header=None)
y = df.iloc[0:100, 4]
y = np.where(y == "Iris-setosa", 1, -1)
x = df.iloc[0:100, [0, 2]].values
# plt.scatter(x[:50, 0], x[:50, 1], color='red', marker='o', label='setosa')
# plt.scatter(x[50:100, 0], x[50:100, 1], color='blue', marker='x', label='versicolor')
# plt.xlabel('petal length')
# plt.ylabel('sepal length')
# plt.legend(loc='upper left')
# plt.show()

from classifiers.percetron import Perceptron
from plot.plot_boundary import plot_decision_region
ppn = Perceptron(eta=0.1, n_iter=10)
ppn.fit(x, y)
# plt.plot(range(1, len(ppn.errors_)+1), ppn.errors_, marker='o')

plot_decision_region(x, y, classfier=ppn)
plt.xlabel('epoch')
plt.ylabel('# of misclassifications')
plt.show()