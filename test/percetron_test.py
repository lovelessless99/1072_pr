from classifiers.percetron import Perceptron
from plot.plot_boundary import plot_decision_region

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def test_percetron():
    df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', sep=',', header=None)
    y = df.iloc[0:100, 4]
    y = np.where(y == "Iris-versicolor", 1, -1)
    x = df.iloc[0:100, [0, 2]].values

    ppn = Perceptron(eta=0.1, n_iter=10)
    ppn.fit(x, y)
    # plt.plot(range(1, len(ppn.errors_)+1), ppn.errors_, marker='o')

    plot_decision_region(x, y, classfier=ppn)
    plt.xlabel('epoch')
    plt.ylabel('# of misclassifications')
    plt.show()


if __name__ == "__main__":
    test_percetron()
# plt.scatter(x[:50, 0], x[:50, 1], color='red', marker='o', label='setosa')
# plt.scatter(x[50:100, 0], x[50:100, 1], color='blue', marker='x', label='versicolor')
# plt.xlabel('petal length')
# plt.ylabel('sepal length')
# plt.legend(loc='upper left')
# plt.show()

