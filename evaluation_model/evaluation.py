import numpy as np
import pandas as pd
import seaborn as sns


class Evaluation:
    def __init__(self, test_data, labels, classifier1, classifier2, classifier3=None):
        self.labels = labels
        self.test_data = test_data
        class_num = len(np.unique(labels))

        self.confusion_matrix1 = np.zeros((class_num, class_num))
        self.confusion_matrix2 = np.zeros((class_num, class_num))
        self.confusion_matrix3 = np.zeros((class_num, class_num))

        self.classifier1 = classifier1
        self.classifier2 = classifier2
        self.classifier3 = classifier3

        self.accuracy1 = 0
        self.accuracy2 = 0
        self.accuracy3 = 0

    def confusion_matrix(self):
        predicted_1 = self.classifier1.predict(self.test_data)
        predicted_2 = self.classifier2.predict(self.test_data)
        predicted_3 = self.classifier3.predict(self.test_data)
        # calculate the confusion matrix; labels is numpy array of classification labels
        for a, p in zip(self.labels, predicted_1):
            self.confusion_matrix1[a][p] += 1

        for a, p in zip(self.labels, predicted_2):
            self.confusion_matrix2[a][p] += 1

        for a, p in zip(self.labels, predicted_3):
            self.confusion_matrix3[a][p] += 1
        # also get the accuracy easily with numpy

        self.accuracy1 = np.mean(self.labels == predicted_1)
        self.accuracy2 = np.mean(self.labels == predicted_2)
        self.accuracy3 = np.mean(self.labels == predicted_2)
        return self

    def plot_confusion(self):
        df_cm1 = pd.DataFrame(self.confusion_matrix1, range(2), range(2))
        df_cm2 = pd.DataFrame(self.confusion_matrix1, range(2), range(2))
        df_cm3 = pd.DataFrame(self.confusion_matrix1, range(2), range(2))
        sns.set(font_scale=1.4)  # for label size
        plt.subplot(131)
        sns.heatmap(df_cm1, annot=True, annot_kws={"size": 16})  # font size

        plt.subplot(132)
        sns.heatmap(df_cm2, annot=True, annot_kws={"size": 16})  # font size

        plt.subplot(133)
        sns.heatmap(df_cm3, annot=True, annot_kws={"size": 16})  # font size
        plt.show()


def roc(actual, pred):
    fpr = np.array([1.0])
    tpr = np.array([1.0])
    n = float(len(actual) - sum(actual))
    p = float(sum(actual))

    for i in np.arange(min(pred), max(pred), 1.0 / len(pred)):
    #for i in np.arange(0, 1, 1.0 / len(pred)):
        TP = 0.0
        FP = 0.0
        for j in range(len(pred)):
            if (pred[j] > i) & (actual[j] == 1):
                TP += 1
            if (pred[j] > i) & (actual[j] == 0):
                FP += 1

        tpr = np.insert(tpr, 0, TP / p)
        fpr = np.insert(fpr, 0, FP / n)
    return fpr, tpr


if __name__ == "__main__":
    # from classifiers.naive_bayes import GaussianNB
    # from sklearn.naive_bayes import GaussianNB
    from classifiers.naive_bayes import GaussianNB
    from classifiers.bayes_classifier import BayesClassifier
    from classifiers.percetron import Perceptron
    from sklearn.datasets import load_iris, load_wine
    from sklearn.model_selection import train_test_split
    import matplotlib.pyplot as plt
    import seaborn as sns

    data = load_wine()
    X_train, X_validation, Y_train, Y_validation = train_test_split(data['data'], data['target'],
                                                                    test_size=0.2,
                                                                    random_state=0)

    Y_train, Y_validation = np.where(Y_train == 0, 1, 0), np.where(Y_validation == 0, 1, 0)

    MGB_predict = BayesClassifier().fit(X_train, Y_train).predict(X_validation)
    nvgb_predict = GaussianNB().fit(X_train, Y_train).predict(X_validation)
    perc_predict = Perceptron().fit(X_train, Y_train).predict(X_validation)

    # MGB = BayesClassifier().fit(X_train, Y_train)
    # GNB = GaussianNB().fit(X_train, Y_train)
    # PCT = Perceptron().fit(X_train, Y_train)

    # Evaluation(X_validation, Y_validation, MGB, GNB, PCT).confusion_matrix().plot_confusion()

    fpr1, tpr1 = roc(Y_validation, MGB_predict)
    fpr2, tpr2 = roc(Y_validation, nvgb_predict)
    fpr,  tpr3 = roc(Y_validation, perc_predict)

    sns.set()
    plt.subplot(131)
    plt.plot(fpr1, tpr1, color='darkorange', lw=2)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc="lower right")
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    plt.subplot(132)
    plt.plot(fpr2, tpr2, color='pink', lw=2)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc="lower right")
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    plt.subplot(133)
    plt.plot(fpr2, tpr2, color='green', lw=2)

    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc="lower right")
    plt.show()


